import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import WeatherDetails from "./views/WeatherDetails.vue";
// import topbar from "./views/WeatherDetails.vue";
import weather from "./components/Weather.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      props: true
    },
    {
      path: "/weather/:id",
      name: "weather_details",
      component: WeatherDetails,
      props: true
    },
    {
      path: "/search/:places",
      name: "search",
      component: weather,
      props: true
    }
  ]
});
